/*
 * @author:她不美却常驻我心
 * @timer:2017-06-28
 * @email:1184112975@qq.com
 * @version:1.3
 * @title:封装一个自己常用的工具类js
 * @note: This js file is for the author's own use only
 */
var wang = {
    /**
     * 获取url参数
     * @param name
     * @returns {*}
     */
    getQueryString : function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if(r != null) return unescape(r[2]);
        return null;
    },
    /**
     * 获取或获取设置时间 YYYY - MM - DD
     * @param Y 年
     * @param M 月
     * @param H 日
     * @returns {string} YYYY - MM - DD
     */
    getData : function (Y,M,H){
        var Y , M , H ;
        if(arguments.length < 3){
            Y = M = H = 0
        }
        var dd = new Date();
        dd.setYear(dd.getFullYear()+Y); // x为当前的
        dd.setMonth(dd.getMonth()+M);
        dd.setDate(dd.getDate()+H);
        var y = dd.getFullYear();
        var m = dd.getMonth()+1;
        var d = dd.getDate();
        return y+"-"+wang.toDub(m)+"-"+wang.toDub(d);
    },
    /**
     * 获得详细时间 YYYY - MM - DD HH : mm : ss
     * @returns {string} YYYY - MM - DD HH : mm : ss
     */
    getTime : function (){
        var d = new Date();
        return d.getFullYear() + "-" + wang.toDub( new Date().getMonth()+1) + "-" + wang.toDub(d.getDate()) + " " + wang.toDub(d.getHours()) + ':' + wang.toDub(d.getMinutes()) + ':' + wang.toDub(d.getSeconds());
    },
    /**
     * 获取年份
     * @returns {number}
     */
    getYear : function (){
        return  new Date().getFullYear();
    },
    /**
     * 获取月份
     * @returns {number}
     */
    getMonth : function (){
        return  new Date().getMonth()+1;
    },
    /**
     *  获取日期
     * @returns {number}
     */
    getDay : function (){
        return  new Date().getDate();
    },
    /**
     * 获取星期
     * @returns {number}
     */
    getWeek : function (){
        return  new Date().getDay();
    },
    /**
     * 获取某个月份的天数
     * @param year 年
     * @param month 月
     * @returns {number}
     */
    getDays : function (year,month){
        return new Date(year, month, 0).getDate();
    },
    /**
     * 转化时间格式为 yyyy - mm - dd hh : mm : ss
     * @param timeStamp 时间戳
     * @returns {string}
     */
    conversionData : function (timeStamp){
        var d = new Date(str);
        var youWant = d.getFullYear() + '-' + wang.toDub(d.getMonth() + 1) + '-' + wang.toDub(d.getDate()) + ' ' + wang.toDub(d.getHours()) + ':' + wang.toDub(d.getMinutes()) + ':' + wang.toDub(d.getSeconds());

        return youWant ;
    },
    /**
     * url 编码
     * @param str
     * @returns {string}
     */
    encodeURL : function (str){
      return window.btoa(wang.encodeURI(str))
    },
    /**
     * url 解码
     * @param str
     * @returns {*}
     */
    decodeURL : function (str){
        return wang.decodeURI(window.atob(wang.getQueryString(str)))
    },
    /**
     * URI 编码
     * @param key
     * @returns {string}
     */
    encodeURI : function (key){
        return encodeURI(key);
    },
    /**
     * URI 解码
     * @param key
     * @returns {string}
     */
    decodeURI : function (key){
        return decodeURI(key);
    },
    /**
     *  json 去重
     * @param array
     * @param key
     * @returns {*[]}
     */
    uniqueArray : function (array,key){
        var result = [array[0]];
        for(var i = 1; i < array.length; i++){
            var item = array[i];
            var repeat = false;
            for (var j = 0; j < result.length; j++) {
                if (item[key] == result[j][key]) {
                    repeat = true;
                    break;
                }
            }
            if (!repeat) {
                result.push(item);
            }
        }
        return result;
    },
    /**
     * 数组去重
     * @param arr
     * @returns {array}
     */
    removeRepeatArray: function(arr) {
        var hash={};
        var data=[];
        for(var i=0;i<arr.length;i++){
            if(!hash[JSON.stringify(arr[i])]){
                hash[JSON.stringify(arr[i])] = true;
                data.push(arr[i]);
            }
        }
        return data;
    },
    /**
     * 设置cookie
     * @param name
     * @param value
     * @param iDay
     */
    setCookie: function(name, value, iDay) {
        var oDate = new Date();
        oDate.setDate(oDate.getDate() + iDay);
        document.cookie = name + '=' + value + ';expires=' + oDate;
    },
    /**
     * 获取cookie
     * @param name
     * @returns {string}
     */
    getCookie: function(name) {
        var arr = document.cookie.split('; ');
        for (var i = 0; i < arr.length; i++) {
            var arr2 = arr[i].split('=');
            if (arr2[0] == name) {
                return arr2[1];
            }
        }
        return '';
    },
    /**
     * 删除cookie
     * @param name
     */
    removeCookie: function(name) {
        this.setCookie(name, 1, -1);
    },
    /**
     * 字符串替换
     * @param str
     * @param AFindText 要替换的字符或者正则表达式（不要写g）
     * @param ARepText 替换成什么
     * @returns {*}
     */
    replaceAll: function(str, AFindText, ARepText) {
        raRegExp = new RegExp(AFindText, "g");
        return str.replace(raRegExp, ARepText);
    },
    /**
     * 过滤字符串(html标签，表情，特殊字符)
     * @param str
     * @param type special-特殊字符,html-html标签,emjoy-emjoy表情,word-小写字母，WORD-大写字母，number-数字,chinese-中文
     * @param restr 过滤字符串的html标签，大写字母，中文，特殊字符，全部替换成*,但是特殊字符'%'，'?'，除了这两个，其他特殊字符全部清除
     * @param spstr 需要保留的
     * @returns {*}
     */
    filterStr: function(str, type, restr, spstr) {
        var typeArr = type.split(','),
            _str = str;
        for (var i = 0, len = typeArr.length; i < len; i++) {
            //是否是过滤特殊符号
            if (typeArr[i] === 'special') {
                var pattern, regText = '$()[]{}?\|^*+./\"\'+';
                //是否有哪些特殊符号需要保留
                if (spstr) {
                    var _spstr = spstr.split(""),
                        _regText = "[^0-9A-Za-z\\s";
                    for (var j = 0, len1 = _spstr.length; j < len1; j++) {
                        if (regText.indexOf(_spstr[j]) === -1) {
                            _regText += _spstr[j];
                        } else {
                            _regText += '\\' + _spstr[j];
                        }
                    }
                    _regText += ']'
                    pattern = new RegExp(_regText, 'g');
                } else {
                    pattern = new RegExp("[^0-9A-Za-z\\s]", 'g')
                }

            }
            var _restr = restr || '';
            switch (typeArr[i]) {
                case 'special':
                    _str = _str.replace(pattern, _restr);
                    break;
                case 'html':
                    _str = _str.replace(/<\/?[^>]*>/g, _restr);
                    break;
                case 'emjoy':
                    _str = _str.replace(/[^\u4e00-\u9fa5|\u0000-\u00ff|\u3002|\uFF1F|\uFF01|\uff0c|\u3001|\uff1b|\uff1a|\u3008-\u300f|\u2018|\u2019|\u201c|\u201d|\uff08|\uff09|\u2014|\u2026|\u2013|\uff0e]/g, _restr);
                    break;
                case 'word':
                    _str = _str.replace(/[a-z]/g, _restr);
                    break;
                case 'WORD':
                    _str = _str.replace(/[A-Z]/g, _restr);
                    break;
                case 'number':
                    _str = _str.replace(/[0-9]/g, _restr);
                    break;
                case 'chinese':
                    _str = _str.replace(/[\u4E00-\u9FA5]/g, _restr);
                    break;
            }
        }
        return _str;
    },
    /**
     * 取随机数
     * @param n 最小
     * @param m 最大
     * @returns {number} 包含最小，不包含最大
     */
    rnd : function (n,m){
        return parseInt(Math.random()*(m-n)+n)
    },
    /**
     * 随机十六进制颜色
     * @returns {string}
     */
    rudColor : function (){
        var colorValue = ["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"];
        var color = "#";
        for (var i = 0; i < colorValue.length; i++) {
            color += colorValue[parseInt(Math.random()*16)]
        }
        return color ;
    },
    /**
     * 补零
     * @param n
     * @returns {string}
     */
    toDub : function (n){
        return n<10?'0'+n:''+n;
    },
    /**
     * 获取行间样式
     * @param obj jquery对象
     * @param name
     * @returns {*}
     */
    getStyle : function (obj,name){
        if(obj instanceof jQuery){
            return obj.attr(name)
        }else{
            alert("传入Jquery对象")
        }
    },
    /**
     * 打开弹窗
     * @param trigger
     * @param popObj
     */
    openWindow : function (trigger,popObj) {
        $(trigger).on("click",function () {
            $(popObj).show();
        })
    },
    /**
     * 关闭弹窗
     * @param trigger
     * @param popObj
     */
    closeWindow : function (trigger,popObj) {
        $(trigger).on("click",function () {
            $(popObj).hide();
        })
    },
    /**
     * 将json按照key排序
     * @param arys
     */
    objKeySort : function (arys) {
        //先用Object内置类的keys方法获取要排序对象的属性名，再利用Array原型上的sort方法对获取的属性名进行排序，newkey是一个数组
        var newkey = Object.keys(arys).sort();
        var newObj = {}; //创建一个新的对象，用于存放排好序的键值对
        for(var i = 0; i < newkey.length; i++) {
            //遍历newkey数组
            newObj[newkey[i]] = arys[newkey[i]];
            //向新创建的对象中按照排好的顺序依次增加键值对

        }
        return newObj; //返回排好序的新对象
    },
    /**
     * b,kb,mb转化
     * @param limit 数值
     * @returns {string}
     */
    conver :  function(limit){
        var size = "";
        if( limit < 0.1 * 1024 ){ //如果小于0.1KB转化成B
            size = limit.toFixed(2) + "B";
        }else if(limit < 0.1 * 1024 * 1024 ){//如果小于0.1MB转化成KB
            size = (limit / 1024).toFixed(2) + "KB";
        }else if(limit < 0.1 * 1024 * 1024 * 1024){ //如果小于0.1GB转化成MB
            size = (limit / (1024 * 1024)).toFixed(2) + "MB";
        }else{ //其他转化成GB
            size = (limit / (1024 * 1024 * 1024)).toFixed(2) + "GB";
        }

        var sizestr = size + "";

        var len = sizestr.indexOf("\.");

        var dec = sizestr.substr(len + 1, 2);

        if(dec == "00"){//当小数点后为00时 去掉小数部分
            return sizestr.substring(0,len) + sizestr.substr(len + 3,2);
        }
        return sizestr;
    },
    /**
     * 点击下拉
     * @param trigger
     * @param child
     */
    chooseList : function ( trigger , child) {
        $(trigger).on("click",function () {
            $(this).addClass("active").siblings().removeClass("active");
            $(trigger).next("dl").toggle();
        });
        $(child).on("click",function () {
            $(trigger).html($(this).html());
            $(child).parent().hide();
        })
    },
    /**
     * PC拖拽
     * @param obj
     */
    pcDrag : function (obj){
        var clW = document.documentElement.clientWidth || document.body.clientWidth;
        var clH = document.documentElement.clientHeight || document.body.clientHeight;
        obj.onmousedown = function (ev){
            var oEvent = ev || event;
            var disX = oEvent.clientX - obj.offsetLeft;
            var disY = oEvent.clientY - obj.offsetTop;
            document.onmousemove = function (ev){
                var oEvent = ev || event;
                var left = oEvent.clientX - disX;
                var top = oEvent.clientY - disY;
                if (left < 0){
                    left = 0;
                }else if (left > clW - obj.offsetWidth){
                    left = clW - obj.offsetWidth;
                }
                if (top < 0){
                    top = 0;
                }else if (top > clH - obj.offsetHeight){
                    top = clH - obj.offsetHeight-1;
                }
                obj.style.left = left+'px';
                obj.style.top = top+'px';
            };
            document.onmouseup = function (){
                document.onmousemove = null;
                document.onmouseup = null;
                obj.releaseCapture && obj.releaseCapture();//releaseCapture释放鼠标信息
            };
            obj.setCapture && obj.setCapture();//setCapture捕获鼠标信息
            return false;
        };
    },
    /**
     * 移动端拖拽
     * @param obj
     */
    mobileDrag : function (obj){

        var disX,moveX,L,T,starX,starY,starXEnd,starYEnd;

        obj.addEventListener('touchstart',function(e){

            e.preventDefault();

            disX = e.touches[0].clientX - this.offsetLeft;
            disY = e.touches[0].clientY - this.offsetTop;

            starX = e.touches[0].clientX;
            starY = e.touches[0].clientY;

        });
        obj.addEventListener('touchmove',function(e){
            L = e.touches[0].clientX - disX ;
            T = e.touches[0].clientY - disY ;

            starXEnd = e.touches[0].clientX - starX;
            starYEnd = e.touches[0].clientY - starY;

            if(L < 0){
                L = 0;
            }else if(L > document.documentElement.clientWidth - this.offsetWidth){
                L = document.documentElement.clientWidth - this.offsetWidth;
            }

            if(T < 0){
                T = 0;
            }else if(T>document.documentElement.clientHeight - this.offsetHeight){
                T = document.documentElement.clientHeight - this.offsetHeight;
            }
            moveX = L + 'px';
            moveY = T + 'px';

            this.style.left = moveX;
            this.style.top = moveY;
        });
        window.addEventListener('touchend',function(e){});
    },
    /**
     * 根据key排序
     *  json = json.sort(sortJsonWidthKey(key,type));
     * @param key
     * @param type asc 升序 desc 降序
     * @returns {(function(*, *): number)|*}
     */
    sortJsonWidthKey : function ( key , type ){
        if( type === "asc" ){
            asc = function(x,y)
            {
                return (Number(x[key]) > Number(y[key]) ) ? 1 : -1
            };
            return asc
        }else{
            desc = function(x,y)
            {
                return (Number(x[key]) < Number(y[key]) ) ? 1 : -1
            };
            return desc
        }

    },
    /**
     * 是否含属性
     * @param obj
     * @param attr
     * @returns {boolean}
     */
    isHasAttr :	function (obj, attr) {
        if (obj && obj.hasOwnProperty(attr)) {
            return true;
        }
        return false;
    },
    /**
     * 选项卡
     * @param btn
     * @param item
     */
    itemTab : function ( btn , item ){
        $(btn).on("click",function () {
            var thisIndex = $(this).index();
            $(this).addClass("active").siblings().removeClass("active");
            $(item).eq(thisIndex).show().siblings().hide();
        })
    },
    /**
     * 获得元素位置
     * @param selector
     * @returns {{top: ((function())|jQuery), left: ((function())|jQuery)}}
     */
    getElementPlace : function ( selector ){
        var place = {
            top : $(selector).offset().top,
            left : $(selector).offset().left
        };
        return place;
    },
    /**
     * 数组排序
     * @param arr
     * @param type
     * @returns {*}
     */
    arrSort : function ( arr , type ) {
        if ( type === "asc" ){
            arr.sort(function (a,b) {
                return Number(a) + Number(b);
            });
        }else{
            arr.sort(function (a,b) {
                return Number(b) - Number(a);
            });
        }
        return arr;
    }
};

